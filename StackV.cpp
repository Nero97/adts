#include "StackV.h"
#include <iostream>

using namespace std;


int top = -1;


void Stack::push(int x)
{
	if (top >= data.size()-1)
	{ 
		cout << "Stack Overflow";
	}
	else 
	{
		data[++top] = x;
		cout << x << "pushed to stack \n";
	}
}

int Stack::size()
{
	return data;
}

void Stack::pop()
{
	if (top < 0)
	{
		cout << "Stack Underflow";
	}
	else
	{
		data[top--];
	}
}

int Stack::top()
{
	if (top < 0)
	{
		cout << "Stack is empty";
		return 0;
	}
	else
	{
		int x = data[top];
		return x;
	}
}
		
